<?php

/**
 * Alter the settings for smart_breadcrumb before processing of the current page.
 *
 * @param array $settings
 *   Settings for Smart Breadcrumb.
 * @return array
 *   New settings to be merged into original.
 */
function hook_smart_breadcrumb_settings($settings) {
}

/**
 * Alters the source path provied for the requested path.
 *
 * @param string $source
 *   The source path for the current crumb.
 */
function hook_crumb_source_alter(&$source) {
}

/**
 * Provide a suggested title for a given path.
 *
 * @param string $path
 *   The requested path.
 * @param string $source
 *   If the path is a valid path alias, the source for the alias.
 * @param string $language
 *   The language the path was requested for.
 * @return string
 *   The title to be used for the provided path.
 */
function hook_crumb_title($path, $source, $lang) {
}

/**
 * Provides default generated title to be used if no other title was found using hook_crumb_title.
 *
 * @param string $path
 *   The requested path.
 * @param string $lang
 *   The language the path was requested for.
 * @return string
 *   The title to be used for the provided path in the event that no other title was found.
 */
function hook_crumb_title_raw($path, $lang) {
}

/**
 * Alters the source path provied for the requested path.
 *
 * @param string $crumb
 *   The crumb after it has been generated through all implementations of hook_crumb_title and markup added.
 * @param array $title
 *   The title and all parameters associated with it.
 * @param array $count
 *   The position which the crumb is located within the breadcrumb.
 */
function hook_crumb_alter(&$crumb, $title, $count) {
}

/**
 * Alter the breadcrumb after all titles have been generated.
 *
 * @param array $breadcrumb
 *   The breadcrumb array to be output.
 * @param string $path
 *   The requested path.
 * @param array $settings
 *   Settings for Smart Breadcrumb.
 */
function hook_breadcrumb_alter(&$breadcrumb, $path, $settings) {
}
